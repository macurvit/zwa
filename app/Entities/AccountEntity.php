<?php

namespace Entities;

use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Doctrine\ORM\Mapping as ORM;

/**
 * Reflects the structure of table 'account' in database.
 *
 * Contains information about the specified account identified by ID
 *
 * @ORM\Entity
 * @ORM\Table(name="account")
 */
class AccountEntity
{

	use Identifier;

	/**
	 * Username of the account
	 *
	 * @var string
	 * @ORM\Column(type="string")
	 */
	protected $username;


	/**
	 * Hashed password of the account
	 *
	 * Hashed using php native function password_hash()
	 *
	 * @var string
	 * @ORM\Column(type="string")
	 */
	protected $password;

	/**
	 * E-Mail address of the account
	 *
	 * @var string
	 * @ORM\Column(type="string")
	 */
	protected $mail;


	/**
	 * Specifies if user is admin
	 *
	 * @var bool
	 * @ORM\Column(type="boolean")
	 */
	protected $admin = false;

	/**
	 * @return string
	 */
	public function getUsername()
	{
		return $this->username;
	}

	/**
	 * @param string $username
	 */
	public function setUsername($username)
	{
		$this->username = $username;
	}

	/**
	 * @return string
	 */
	public function getPassword()
	{
		return $this->password;
	}

	/**
	 * @param string $password
	 */
	public function setPassword($password)
	{
		$this->password = $password;
	}

	/**
	 * @return string
	 */
	public function getMail()
	{
		return $this->mail;
	}

	/**
	 * @param string $mail
	 */
	public function setMail($mail)
	{
		$this->mail = $mail;
	}

	/**
	 * @return bool
	 */
	public function isAdmin()
	{
		return $this->admin;
	}

	/**
	 * @param bool $admin
	 */
	public function setAdmin($admin)
	{
		$this->admin = $admin;
	}

}
