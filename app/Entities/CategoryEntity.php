<?php

namespace Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Doctrine\ORM\Mapping as ORM;

/**
 * Reflects the structure of table 'category' in database.
 *
 * Contains information about the specified category identified by ID
 *
 * @ORM\Entity
 * @ORM\Table(name="category")
 */
class CategoryEntity
{

	use Identifier;

	/**
	 * Abbreviation of the category
	 *
	 * @var string
	 * @ORM\Column(type="string")
	 */
	protected $abbr;

	/**
	 * Name of the category
	 *
	 * @var string
	 * @ORM\Column(type="string")
	 */
	protected $name;

	/**
	 * Products assigned to this category
	 *
	 * @var ProductEntity[]|Collection
	 * @ORM\ManyToMany(targetEntity="ProductEntity", inversedBy="categories")
	 * @ORM\JoinTable(
	 *     name="category_product",
	 *     joinColumns={
	 *         @ORM\JoinColumn(name="category_id", referencedColumnName="id")
	 *     },
	 *     inverseJoinColumns={
	 *         @ORM\JoinColumn(name="product_id", referencedColumnName="id")
	 *     }
	 * )
	 */
	protected $products;

	/**
	 * Initializes new CategoryEntity
	 */
	public function __construct()
	{
		$this->products = new ArrayCollection();
	}

	/**
	 * @return string
	 */
	public function getAbbr()
	{
		return $this->abbr;
	}

	/**
	 * @param string $abbr
	 */
	public function setAbbr(string $abbr)
	{
		$this->abbr = $abbr;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name)
	{
		$this->name = $name;
	}

	/**
	 * @return ProductEntity[]|ArrayCollection
	 */
	public function getProducts()
	{
		return $this->products;
	}

	/**
	 * @param ProductEntity[] $products
	 */
	public function setProducts($products)
	{
		$this->products = $products;
	}

	/**
	 * Associates given product with this category
	 *
	 * @param ProductEntity $product
	 */
	public function addProduct(ProductEntity $product)
	{
		if ($this->products->contains($product))
			return;

		$this->products->add($product);
	}

	/**
	 * Removes given product's association with this category
	 *
	 * @param ProductEntity $product
	 */
	public function removeProduct(ProductEntity $product)
	{
		if (!$this->products->contains($product))
			return;

		$this->products->removeElement($product);
	}

}
