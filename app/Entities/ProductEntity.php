<?php

namespace Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Nette\Utils\DateTime;

/**
 * Reflects the structure of table 'product' in database.
 *
 * Contains information about the specified product identified by ID
 *
 * @ORM\Entity
 * @ORM\Table(name="product")
 */
class ProductEntity
{

	use Identifier;

	/**
	 * Abbreviation of the product
	 *
	 * @var string
	 * @ORM\Column(type="string")
	 */
	protected $abbr;

	/**
	 * Name of the product
	 *
	 * @var string
	 * @ORM\Column(type="string")
	 */
	protected $name;

	/**
	 * Description of the product
	 *
	 * @var string
	 * @ORM\Column(type="text")
	 */
	protected $description;

	/**
	 * Price of the product
	 *
	 * @var float
	 * @ORM\Column(type="float")
	 */
	protected $price;

	/**
	 * Release date of the product
	 *
	 * @var DateTime
	 * @ORM\Column(type="datetime")
	 */
	protected $releaseDate;

	/**
	 * Date of running out of stock for this product
	 *
	 * @var null|DateTime
	 * @ORM\Column(type="datetime")
	 */
	protected $stockEmptyDate = null;

	/**
	 * Date of cancelling the product
	 *
	 * @var null|DateTime
	 * @ORM\Column(type="datetime")
	 */
	protected $cancelDate = null;

	/**
	 * Product's image extension
	 *
	 * @var null|string
	 * @ORM\Column(type="string")
	 */
	protected $imageExt = null;

	/**
	 * Categories assigned to the product
	 *
	 * @var CategoryEntity[]|Collection
	 * @ORM\ManyToMany(targetEntity="CategoryEntity", inversedBy="products", indexBy="id")
	 * @ORM\JoinTable(
	 *     name="category_product",
	 *     joinColumns={
	 *         @ORM\JoinColumn(name="product_id", referencedColumnName="id")
	 *     },
	 *     inverseJoinColumns={
	 *         @ORM\JoinColumn(name="category_id", referencedColumnName="id")
	 *     }
	 * )
	 */
	protected $categories;

	/**
	 * Initializes new ProductEntity
	 */
	public function __construct()
	{
		$this->categories = new ArrayCollection();
	}

	/**
	 * @return string
	 */
	public function getAbbr()
	{
		return $this->abbr;
	}

	/**
	 * @param string $abbr
	 */
	public function setAbbr($abbr)
	{
		$this->abbr = $abbr;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}

	/**
	 * @return float
	 */
	public function getPrice()
	{
		return $this->price;
	}

	/**
	 * @param float $price
	 */
	public function setPrice($price)
	{
		$this->price = $price;
	}

	/**
	 * @return DateTime
	 */
	public function getReleaseDate()
	{
		return $this->releaseDate;
	}

	/**
	 * @param DateTime $releaseDate
	 */
	public function setReleaseDate($releaseDate)
	{
		$this->releaseDate = $releaseDate;
	}


	/**
	 * @return bool
	 */
	public function isAvailable()
	{
		return $this->stockEmptyDate === null;
	}

	/**
	 * @return DateTime|null
	 */
	public function getStockEmptyDate()
	{
		return $this->stockEmptyDate;
	}

	/**
	 * @param DateTime|null $stockEmptyDate
	 */
	public function setStockEmptyDate($stockEmptyDate)
	{
		$this->stockEmptyDate = $stockEmptyDate;
	}

	/**
	 * @return bool
	 */
	public function isCanceled()
	{
		return $this->cancelDate !== null;
	}

	/**
	 * @return DateTime|null
	 */
	public function getCancelDate()
	{
		return $this->cancelDate;
	}

	/**
	 * @param DateTime|null $cancelDate
	 */
	public function setCancelDate($cancelDate)
	{
		$this->cancelDate = $cancelDate;
	}

	/**
	 * @return null|string
	 */
	public function getImageExt()
	{
		return $this->imageExt;
	}

	/**
	 * @param null|string $imageExt
	 */
	public function setImageExt($imageExt)
	{
		$this->imageExt = $imageExt;
	}

	/**
	 * @return CategoryEntity[]
	 */
	public function getCategories()
	{
		return $this->categories;
	}

	/**
	 * @param CategoryEntity[] $categories
	 */
	public function setCategories($categories)
	{
		$this->categories = $categories;
	}

	/**
	 * Associates given category with this product
	 *
	 * @param CategoryEntity $category
	 */
	public function addCategory(CategoryEntity $category)
	{
		if ($this->categories->contains($category))
			return;

		$this->categories->add($category);
	}

	/**
	 * Removes the given category's association with this product
	 *
	 * @param CategoryEntity $category
	 */
	public function removeCategory(CategoryEntity $category)
	{
		if (!$this->categories->contains($category))
			return;

		$this->categories->removeElement($category);
	}

	/**
	 * Checks if the given category is associated with this product
	 *
	 * @param CategoryEntity|int $category The category to check for
	 * @return bool True if category is associated, else false
	 */
	public function hasCategory($category)
	{
		if ($category instanceof CategoryEntity)
			return $this->categories->contains($category);

		return $this->categories->containsKey($category);
	}

}
