<?php

namespace Entities;

use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Doctrine\ORM\Mapping as ORM;

/**
 * Reflects the structure of table 'register_token' in database.
 *
 * Contains information about the specified token identified by ID
 *
 * @ORM\Entity
 * @ORM\Table(name="register_token")
 */
class RegisterTokenEntity
{

	use Identifier;

	/**
	 * The token string
	 *
	 * @var string
	 * @ORM\Column(type="string")
	 */
	protected $token;

	/**
	 * Specifies if token has been used
	 *
	 * @var bool
	 * @ORM\Column(type="boolean")
	 */
	protected $used = false;

	/**
	 * @return string
	 */
	public function getToken()
	{
		return $this->token;
	}

	/**
	 * @param string $token
	 */
	public function setToken($token)
	{
		$this->token = $token;
	}

	/**
	 * @return bool
	 */
	public function isUsed()
	{
		return $this->used;
	}

	/**
	 * @param bool $used
	 */
	public function setUsed($used)
	{
		$this->used = $used;
	}

}
