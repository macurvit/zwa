<?php

namespace Models;

use Entities\AccountEntity;
use Entities\RegisterTokenEntity;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;
use Nette\SmartObject;
use Nette\Utils\ArrayHash;
use Nette\Utils\Random;

/**
 * Provides functionality to work with accounts and tokens (AccountEntity and RegisterTokenEntity)
 */
class Account
{

	use SmartObject;

	/**
	 * Database repositories
	 *
	 * @var EntityRepository
	 */
	protected $repository, $tokenRepository;

	/**
	 * Initializes new Account
	 *
	 * Injects entity repositories to its variables
	 *
	 * @param EntityManager $em
	 */
	public function __construct(EntityManager $em)
	{
		$this->repository = $em->getRepository(AccountEntity::class);
		$this->tokenRepository = $em->getRepository(RegisterTokenEntity::class);
	}

	/**
	 * Checks if an account with the given $value in field $field already exists
	 *
	 * @param string $field Field to search duplicity in
	 * @param string $value Value for which to search
	 * @return bool True if given value would be duplicate, else false
	 * @throws \Exception If field is of a not allowed value
	 */
	public function isDuplicate($field, $value)
	{
		if (in_array($field, ['mail', 'username']))
			return $this->repository->findOneBy([$field => $value]) !== null;
		else
			throw new \Exception('Requested check for unsupported field');
	}

	/**
	 * Checks if any accounts already exist
	 *
	 * @return bool True if no accounts are present, else false
	 */
	public function isFirst()
	{
		return empty($this->repository->findAll());
	}

	/**
	 * Finds account by given $id
	 *
	 * @param int $id ID to search for
	 * @return null|object|AccountEntity Corresponding account if found, null otherwise
	 */
	public function find($id)
	{
		return $this->repository->find($id);
	}

	/**
	 * Returns all existing accounts
	 *
	 * @return array|AccountEntity[]
	 */
	public function findAllAccounts()
	{
		return $this->repository->findAll();
	}


	/**
	 * Finds the first account matching given $criteria sorted by ID
	 *
	 * @param array $criteria Criteria to search for
	 * @return AccountEntity|null Corresponding account if found, null otherwise
	 */
	public function findOneBy($criteria)
	{
		return $this->repository->findOneBy($criteria);
	}

	/**
	 * Saves given $account represented by its entity to database
	 *
	 * @param AccountEntity $account Account to save
	 * @throws \Exception If there is an error when communicating with database
	 */
	public function save(AccountEntity $account)
	{
		$em = $this->repository->getEntityManager();
		$em->persist($account);
		$em->flush();
	}

	/**
	 * Updates an account specified by $id to match given values $values
	 *
	 * @param int $id ID of updated account
	 * @param ArrayHash $values Values to set
	 * @return bool True on success, else false
	 * @throws \Exception If there is an error when communicating with database
	 */
	public function update($id, $values)
	{
		$account = $this->find($id);
		if ($account === null)
			return false;

		if ($values->mail !== $account->getMail() && $this->isDuplicate('mail', $values->mail))
			return false;

		$account->setMail($values->mail);
		$account->setAdmin($values->admin);

		$this->save($account);

		return true;
	}

	/**
	 * Removes an account specified by $id from database
	 *
	 * @param int $id ID of removed account
	 * @return bool True on success, else false
	 * @throws \Exception If there is an error when communicating with database
	 */
	public function remove($id)
	{
		$account = $this->find($id);
		if ($account === null)
			return false;

		$em = $this->repository->getEntityManager();
		$em->remove($account);
		$em->flush();

		return true;
	}

	/* TOKENS */

	/**
	 * Fetches a token entity specified by given token $token
	 *
	 * @param string $token The string to look for
	 * @return null|object|RegisterTokenEntity Corresponding token if found, null otherwise
	 */
	public function getToken($token)
	{
		return $this->tokenRepository->findOneBy(['token' => $token]);
	}


	/**
	 * Generates a new token and saves it to database
	 *
	 * @return string The newly generated token
	 * @throws \Exception If there is an error when saving the token to database
	 */
	public function generateToken()
	{
		$string = Random::generate(32);

		$token = new RegisterTokenEntity();
		$token->setToken($string);

		$this->saveToken($token);

		return $string;
	}

	/**
	 * Sets the token as used
	 *
	 * @param int $id The id to look for
	 * @return bool True on success, else false
	 * @throws \Exception If there is an error when saving the token to database
	 */
	public function removeToken($id)
	{
		$token = $this->findToken($id);
		if ($token === null)
			return false;

		$token->setUsed(true);
		$this->saveToken($token);

		return true;
	}

	/**
	 * Finds token by given $id
	 *
	 * @param int $id The id to look for
	 * @return null|object|RegisterTokenEntity Corresponding token if found, null otherwise
	 */
	public function findToken($id)
	{
		return $this->tokenRepository->find($id);
	}

	/**
	 * Returns all existing tokens
	 *
	 * @return array|RegisterTokenEntity[]
	 */
	public function findAllActiveTokens()
	{
		return $this->tokenRepository->findBy(['used' => false]);
	}

	/**
	 * Saves given token to database
	 *
	 * @param RegisterTokenEntity $token Token to save
	 * @throws \Exception If there is an error when communicating with database
	 */
	public function saveToken(RegisterTokenEntity $token)
	{
		$em = $this->tokenRepository->getEntityManager();
		$em->persist($token);
		$em->flush();
	}

}
