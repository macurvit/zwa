<?php

namespace Models;

use Entities\AccountEntity;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\SmartObject;

/**
 * Provides user authentication for the application
 */
class Authenticator implements IAuthenticator
{

	use SmartObject;

	/**
	 * Account database repository
	 *
	 * @var EntityRepository
	 */
	private $account;

	/**
	 * Initialize new Authenticator
	 *
	 * Injects the account repository class
	 *
	 * @param EntityManager $em
	 */
	public function __construct(EntityManager $em)
	{
		$this->account = $em->getRepository(AccountEntity::class);
	}

	/**
	 * Authenticates a user to login
	 *
	 * @param array $credentials Username and password
	 * @return Identity|\Nette\Security\IIdentity
	 * @throws AuthenticationException On authentication failure - account not found or password doesn't match
	 */
	public function authenticate(array $credentials)
	{
		list($identifier, $password) = $credentials;

		$account = $this->account->findOneBy(['username' => $identifier]);

		if (!$account) {
			throw new AuthenticationException();
		}

		$authorized = false;

		if (password_verify($password, $account->getPassword()))
			$authorized = true;

		if (!$authorized)
			throw new AuthenticationException();

		$group = $account->isAdmin() ? 'admin' : 'guest';
		$data = ['logged' => new \DateTime()];

		return new Identity($account->getId(), $group, $data);
	}

}
