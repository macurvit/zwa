<?php

namespace Models;

use Kdyby\Doctrine\EntityManager;
use Entities\CategoryEntity;
use Kdyby\Doctrine\EntityRepository;
use Nette\SmartObject;
use Nette\Utils\ArrayHash;

/**
 * Provides functionality to work with categories (CategoryEntity)
 */
class Category
{

	use SmartObject;

	/**
	 * Database repository
	 *
	 * @var EntityRepository
	 */
	protected $repository;

	/**
	 * Initializes new Category
	 *
	 * Injects the category repository
	 *
	 * @param EntityManager $em
	 */
	public function __construct(EntityManager $em)
	{
		$this->repository = $em->getRepository(CategoryEntity::class);
	}

	/**
	 * Creates a new category and saves the new category to database
	 *
	 * @param ArrayHash $values Values to set to the new category
	 * @return bool True on success
	 * @throws \Exception If there is an error when saving category to database
	 */
	public function addCategory($values)
	{
		$category = new CategoryEntity();

		$category->setAbbr($values->abbr);
		$category->setName($values->name);

		$this->saveCategory($category);

		return true;
	}

	/**
	 * Updates category identified by $id to match given values
	 *
	 * @param int $id The id of category to edit
	 * @param ArrayHash $values New values to set
	 * @return bool True on success, else false
	 * @throws \Exception If there is an error when saving category to database
	 */
	public function updateCategory($id, $values)
	{
		$category = $this->find($id);
		if ($category === null)
			return false;

		if ($values->abbr !== $category->getAbbr() && $this->isDuplicate($values->abbr))
			return false;

		$category->setAbbr($values->abbr);
		$category->setName($values->name);

		$this->saveCategory($category);

		return true;
	}

	/**
	 * Removes category identified by $id from database
	 *
	 * @param int $id The id to look for
	 * @return bool True on success, else false
	 * @throws \Exception If there is an error when communicating with database
	 */
	public function removeCategory($id)
	{
		$category = $this->find($id);
		if ($category === null)
			return false;

		$em = $this->repository->getEntityManager();
		$em->remove($category);
		$em->flush();

		return true;
	}


	/**
	 * Saves given category to database
	 *
	 * @param CategoryEntity $category Category to save
	 * @throws \Exception If there is an error when communicating with database
	 */
	public function saveCategory(CategoryEntity $category)
	{
		$em = $this->repository->getEntityManager();
		$em->persist($category);
		$em->flush();
	}

	/**
	 * Checks if the $abbr abbreviation is already assigned to a category
	 *
	 * @param string $abbr The abbreviation to check duplicity for
	 * @return bool True if abbreviation already exists, else false
	 */
	public function isDuplicate($abbr)
	{
		return $this->repository->findOneBy(['abbr' => $abbr]) !== null;
	}

	/**
	 * Find categories by an array of $ids
	 *
	 * @param array $ids The IDs to look for
	 * @return CategoryEntity[]|array Found categories
	 */
	public function findByIds($ids)
	{
		return $this->repository->findBy(['id' => $ids]);
	}

	/**
	 * Finds a category by given $id
	 *
	 * @param int $id The id to look for
	 * @return null|CategoryEntity|object Found category, null if none were found
	 */
	public function find($id)
	{
		return $this->repository->find($id);
	}

	/**
	 * Returns all the existing categories
	 *
	 * @return array|CategoryEntity[]
	 */
	public function findAll()
	{
		return $this->repository->findAll();
	}

	/**
	 * Returns all the existing categories as an associative array
	 * @return array Array of categories in format ID => Name
	 */
	public function findAllAsArray()
	{
		$return = [];

		foreach ($this->findAll() as $key => $category) {
			$return[$category->getId()] = $category->getName();
		}

		return $return;
	}

}
