<?php

namespace Models;

use Kdyby\Doctrine\EntityManager;
use Entities\ProductEntity;
use Kdyby\Doctrine\EntityRepository;
use Nette\Http\FileUpload;
use Nette\SmartObject;
use Nette\Utils\ArrayHash;use Nette\Utils\DateTime;

/**
 * Provides functionality to work with products (ProductEntity)
 */
class Product
{

	use SmartObject;

	/**
	 * Database repository
	 *
	 * @var EntityRepository
	 */
	protected $repository;

	/**
	 * Model for interaction with categories
	 *
	 * @var Category
	 */
	protected $category;

	/**
	 * Initializes a new product
	 *
	 * Injects the category class and product repository
	 *
	 * @param Category $category
	 * @param EntityManager $em
	 */
	public function __construct(Category $category, EntityManager $em)
	{
		$this->category = $category;
		$this->repository = $em->getRepository(ProductEntity::class);
	}

	/**
	 * Checks if the $abbr abbreviation is already assigned to a product
	 *
	 * @param string $abbr The abbreviation to check duplicity for
	 * @return bool True if abbreviation already exists, else false
	 */
	public function isDuplicate($abbr)
	{
		return $this->repository->findOneBy(['abbr' => $abbr]) !== null;
	}

	/**
	 * Creates a new product and saves it to database
	 *
	 * While creating a product also moves its image to appropriate folder (/www/images/products/)
	 *
	 * @param ArrayHash $values Values to set to the new Product
	 * @return bool True if product is successfully saved, else false
	 * @throws \Exception If there is an error when saving new product to database
	 */
	public function addProduct($values)
	{
		$product = new ProductEntity();

		$categories = $this->category->findByIds($values->categories);

		$product->setAbbr($values->abbr);
		$product->setName($values->name);
		$product->setDescription($values->description);
		$product->setPrice((float)$values->price);
		$product->setReleaseDate(new DateTime());

		if (isset($values->image)) {
			if ($values->image instanceof FileUpload && $values->image->isOk()) {
				$fileType = explode('.', $values->image->getName())[1];
				$values->image->move(__BASEDIR__ . '/www/images/products/' . $values->abbr . '.' . $fileType);
				$product->setImageExt($fileType);
			}
		} else
			return false;

		foreach ($categories as $category) {
			$product->addCategory($category);
		}

		$this->saveProduct($product);

		return true;
	}

	/**
	 * Updates product identified by $id to match the new $values
	 *
	 * @param int $id Id of the product to update
	 * @param ArrayHash $values The new values to set to the product
	 * @return bool True if successfully saved, else false
	 * @throws \Exception If there is an error when saving the product to database
	 */
	public function updateProduct($id, $values)
	{
		$product = $this->find($id);
		if ($product === null)
			return false;

		if (isset($values->image)) {
			if ($values->image instanceof FileUpload && $values->image->isOk()) {
				$fileType = explode('.', $values->image->getName())[1];
				$values->image->move(__BASEDIR__ . '/www/images/products/' . $values->abbr . '.' . $fileType);
				$product->setImageExt($fileType);
			}
		} else
			return false;

		$product->setName($values->name);
		$product->setPrice($values->price);
		$product->setDescription($values->description);
		foreach ($product->getCategories() as $category) {
			if (!in_array($category->getId(), $values->categories))
				$product->removeCategory($category);
		}
		foreach ($values->categories as $category) {
			if (!$product->hasCategory($category))
				$product->addCategory($this->category->find($category));
		}

		$this->saveProduct($product);

		return true;
	}


	/**
	 * Sets the cancel date of product specified by $id to NOW
	 *
	 * @param int $id Id of the product to remove
	 * @return bool True on success, else false
	 * @throws \Exception If there is an error when saving the product to database
	 */
	public function removeProduct($id)
	{
		$product = $this->find($id);
		if ($product === null)
			return false;

		$product->setCancelDate(new DateTime());
		$this->saveProduct($product);

		return true;
	}

	/**
	 * Marks a product at not available in store
	 *
	 * @param int $id Id of the product to mark unavailable
	 * @return bool True on success, else false
	 * @throws \Exception If there is an error when saving the product to database
	 */
	public function markUnavailable($id)
	{
		$product = $this->find($id);
		$product->setStockEmptyDate(new DateTime());
		$this->saveProduct($product);

		return true;
	}

	/**
	 * Marks a product as available in store
	 *
	 * @param int $id Id of the product to mark available
	 * @return bool True on success, else false
	 * @throws \Exception If there is an error when saving the product to database
	 */
	public function markAvailable($id)
	{
		$product = $this->find($id);
		$product->setStockEmptyDate(null);
		$this->saveProduct($product);

		return true;
	}

	/**
	 * Returns all existing products including the canceled ("removed") ones
	 *
	 * @return array|ProductEntity[]
	 */
	public function findAll()
	{
		return $this->repository->findBy([], ['releaseDate' => 'DESC']);
	}

	/**
	 * Returns products matching given criteria
	 *
	 * Products are filtered and sorted by $filters, returns a number of $limit products starting at $offset
	 *
	 * @param int $limit Number of products to return
	 * @param int $offset Offset at which start fetching products
	 * @param array $filters Criteria to filter the selection by
	 * @return array|ProductEntity[] Products that matched the criteria
	 */
	public function findActiveProducts($limit, $offset, $filters)
	{
		$qb = $this->prepareQueryBuilder($filters);

		$qb->select('p');
		$qb->setFirstResult($offset)->setMaxResults($limit);

		return $qb->getQuery()->getResult();
	}

	/**
	 * Finds a product by given $id
	 *
	 * @param int $id The id to look for
	 * @return null|object|ProductEntity
	 */
	public function find($id)
	{
		return $this->repository->find($id);
	}

	/**
	 * Returns products to show on homepage
	 *
	 * @return array|ProductEntity[] The products to show
	 */
	public function getHomepageProducts()
	{
		return $this->repository->createQueryBuilder('p')
			->select('p')
			->where('p.cancelDate IS NULL')
			->andWhere('p.stockEmptyDate IS NULL')
			->andWhere('p.imageExt IS NOT NULL')
			->orderBy('p.releaseDate','DESC')
			->setMaxResults(4)
			->getQuery()->getResult();
	}

	/**
	 * Saves given product to database
	 *
	 * @param ProductEntity $product Product to save
	 * @throws \Exception If there is an error when communicating with the database
	 */
	public function saveProduct(ProductEntity $product)
	{
		$em = $this->repository->getEntityManager();
		$em->persist($product);
		$em->flush();
	}

	/**
	 * Returns number of products matching the given criteria
	 *
	 * Does not throw IDE-reported exceptions as 'SELECT COUNT()' is always 'SingleScalarResult'
	 *
	 * @param array $filters The criteria to match
	 * @return int
	 */
	public function getActiveProductsCount($filters = [])
	{
		return $this->prepareQueryBuilder($filters)->select('COUNT(p.id)')->getQuery()->getSingleScalarResult();
	}

	/**
	 * Returns the statistics to show on admin dashboard
	 *
	 * Does not throw IDE-reported exceptions as 'SELECT COUNT()' is always 'SingleScalarResult'
	 *
	 * @return array The statistics
	 */
	public function getStats()
	{
		$countAll = $this->repository->createQuery('SELECT COUNT(p.id) FROM Entities\ProductEntity p')->getSingleScalarResult();
		$countActive = $this->getActiveProductsCount();
		$countInactive = $countAll - $countActive;
		$countAvailable = $this->repository->createQuery('SELECT COUNT(p.id) FROM Entities\ProductEntity p WHERE p.stockEmptyDate IS NULL AND p.cancelDate IS NULL')->getSingleScalarResult();
		$countUnavailable = $countActive - $countAvailable;
		return ['all' => $countAll, 'active' => $countActive, 'inactive' => $countInactive, 'available' => $countAvailable, 'unavailable' => $countUnavailable];
	}

	/**
	 * Creates a QueryBuilder instance with given criteria for further use
	 *
	 * @param array $filters The criteria to match
	 * @return \Kdyby\Doctrine\QueryBuilder
	 */
	private function prepareQueryBuilder($filters) {
		$qb = $this->repository->createQueryBuilder('p');

		if (empty($filters))
			return $qb;

		if ($filters['category'] !== 0) $qb->innerJoin('p.categories', 'c', 'WITH', 'c.id = :categories')->setParameter('categories', $filters['category']);
		if ($filters['available'] === true) $qb->andWhere('p.stockEmptyDate IS NULL');
		if ($filters['priceStart'] !== null) $qb->andWhere('p.price >= :priceStart')->setParameter('priceStart', $filters['priceStart']);
		if ($filters['priceEnd'] !== null) $qb->andWhere('p.price <= :priceEnd')->setParameter('priceEnd', $filters['priceEnd']);
		if (in_array($filters['orderBy'], ['releaseDate', 'price', 'stockEmptyDate'])) $qb->orderBy('p.' . $filters['orderBy'], in_array($filters['orderType'], ['DESC', 'ASC']) ? $filters['orderType'] : 'DESC');
		else $qb->orderBy('releaseDate', 'DESC');

		return $qb;
	}

}
