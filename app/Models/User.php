<?php

namespace Models;

use Entities\AccountEntity;
use Nette;

/**
 * Basic User class
 */
class User extends Nette\Security\User
{

	/**
	 * The user's account entity from database
	 *
	 * @var AccountEntity
	 */
	private $accountEntity;

	/**
	 * Initialize a new User instance
	 * @param Nette\Security\IUserStorage $storage
	 * @param Nette\Security\IAuthenticator|null $authenticator
	 * @param Nette\Security\IAuthorizator|null $authorizator
	 */
	public function __construct(Nette\Security\IUserStorage $storage, Nette\Security\IAuthenticator $authenticator = null, Nette\Security\IAuthorizator $authorizator = null)
	{
		parent::__construct($storage, $authenticator, $authorizator);
	}

	/**
	 * Login a user with given parameters - username and password
	 * @param null|string|int $id
	 * @param null|string $password
	 * @throws Nette\Security\AuthenticationException
	 */
	public function login($id = null, $password = null)
	{
		parent::login($id, $password);
	}

	/**
	 * @param AccountEntity $accountEntity
	 */
	public function setAccountEntity(AccountEntity $accountEntity)
	{
		if($accountEntity !== null)
			$this->accountEntity = $accountEntity;
	}

	/**
	 * @return AccountEntity
	 */
	public function getAccountEntity() {
		return $this->accountEntity;
	}

}
