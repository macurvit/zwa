<?php

namespace Modules\Admin;

use Models\Category;
use Models\Product;

/**
 * Controls services required for most child Presenters
 */
class BasePresenter extends \GlobalPresenter
{

	/**
	 * Model for interaction with products
	 *
	 * @var Product
	 */
	protected $product;

	/**
	 * Model for interaction with categories
	 *
	 * @var Category
	 */
	protected $category;

	/**
	 * Redirects guests to homepage
	 */
	public function startup()
	{
		parent::startup();

		if (!$this->getUser()->isLoggedIn())
			$this->redirectHome();
	}

	/**
	 * Injects commonly used services by child presenters
	 *
	 * @param Product $product
	 * @param Category $category
	 */
	public function injectCommonServices(Product $product, Category $category)
	{
		$this->product = $product;
		$this->category = $category;
	}

}
