<?php

namespace Modules\Admin;

use Nette\Application\UI\Form;

/**
 * Controls category manipulation on admin side
 */
class CategoryPresenter extends BasePresenter
{

	/**
	 * Renders the default view
	 */
	public function renderDefault()
	{
		$this->template->categories = $this->category->findAll();
	}

	/**
	 * Renders view to edit a category
	 *
	 * @param int $id Id of the category to edit
	 * @throws \Nette\Application\AbortException On redirect
	 */
	public function renderEdit($id)
	{
		$this->template->id = $id;
		$category = $this->category->find($id);
		if ($category === null) {
			$this->flashMessage('Kategorie  #' . $id . ' nenalezena', 'danger');
			$this->redirect('default');
		}

		/** @var Form $form */
		$form = $this->getComponent('editCategory');
		$form->setDefaults([
			'abbr' => $category->getAbbr(),
			'name' => $category->getName(),
		]);
	}

	/**
	 * Removes category with given $id
	 *
	 * @param int $id Id of the category to remove
	 * @throws \Exception When removing fails due to database problems
	 * @throws \Nette\Application\AbortException On redirect
	 */
	public function actionRemove($id)
	{
		$category = $this->category->find($id);
		if ($category === null) {
			$this->flashMessage('Kategorie  #' . $id . ' nenalezena', 'danger');
			$this->redirect('default');
		}

		if ($this->category->removeCategory($id))
			$this->flashMessage('Kategorie byla úspěšně smazána', 'success');
		else
			$this->flashMessage('Při mazání kategorie došlo k chybě', 'danger');

		$this->redirect('default');
	}

	/**
	 * Creates a form for adding a category
	 *
	 * @return Form
	 */
	public function createComponentAddCategory()
	{
		return $this->createCategoryForm('addCategory', 'Přidat');
	}

	/**
	 * Handles successful "addCategory" form submitting
	 *
	 * Fetches the form data and if it's not duplicate, saves new category to database
	 *
	 * @param Form $form Submitted form
	 * @throws \Exception When adding category fails due to database problems
	 * @throws \Nette\Application\AbortException On redirect
	 */
	public function addCategory(Form $form)
	{
		$values = $form->getValues();

		if ($this->category->addCategory($values)) {
			$this->flashMessage('Kategorie úspěšně přidána', 'success');
			$this->redirect('this');
		} else
			$this->flashMessage('Při ukládání kategorie došlo k chybě', 'danger');
	}

	/**
	 * Creates a form for editing a category
	 *
	 * @return Form
	 */
	public function createComponentEditCategory()
	{
		return $this->createCategoryForm('editCategory', 'Uložit');
	}

	/**
	 * Handles successful "editCategory" form submitting
	 *
	 * Fetches the form data and updates corresponding category to match new data, saves to database
	 *
	 * @param Form $form Submitted form
	 * @throws \Exception When updating category fails due to database problems
	 * @throws \Nette\Application\AbortException On redirect
	 */
	public function editCategory(Form $form)
	{
		$values = $form->getValues();

		if ($this->category->updateCategory($this->getParameter('id'), $values)) {
			$this->flashMessage('Kategorie úspěšně upravena', 'success');
			$this->redirect('default');
		} else {
			$this->flashMessage('Došlo k chybě při ukládání', 'danger');
		}
	}

	/**
	 * Creates the base category form for both adding and editing
	 *
	 * @param string $successCallback Name of the function to call on successful form submit
	 * @param string $submitName Value of submit input in form
	 * @return Form The created form
	 */
	private function createCategoryForm($successCallback, $submitName)
	{
		$form = new Form();

		$form->addText('abbr', 'Zkratka')
			->setRequired('Musíte zadat zkratku')
			->addRule(Form::MAX_LENGTH, 'Zkratka může být dlouhá maximálně 32 znaků', 32)
			->addRule(Form::PATTERN, 'Pro zkratku požijte pouze znaky A-Z, a-z, 0-9 a pomlčku', '[A-Za-z0-9-]+')
			->setAttribute('placeholder', 'domov');

		$form->addText('name', 'Název')
			->setRequired('Musíte zadat jméno')
			->addRule(Form::MAX_LENGTH, 'Název může být dlouhý maximálně 64 znaků', 64)
			->setAttribute('placeholder', 'Domácí potřeby');

		$form->addSubmit('submit', $submitName);
		$form->addProtection('Platnost formuláře vypršela');

		$form->onValidate[] = [$this, 'validateDuplicity'];
		$form->onSuccess[] = [$this, $successCallback];

		return $form;
	}


	/**
	 * Validates submitted form for abbreviation duplicity
	 *
	 * @param Form $form The submitted form
	 * @return Form The submitted form with checked errors for duplicity
	 */
	public function validateDuplicity(Form $form)
	{
		$values = $form->getValues();
		if ($form->getName() === 'addCategory') {
			if ($this->category->isDuplicate($values->abbr))
				$form->addError('Kategorie s touto zkratkou již existuje');
		} elseif ($form->getName() === 'editCategory') {
			$category = $this->category->find($this->getParameter('id'));
			if ($category->getAbbr() !== $values->abbr && $this->category->isDuplicate($values->abbr))
				$form->addError('Kategorie s touto zkratkou již existuje');
		}

		return $form;
	}

}
