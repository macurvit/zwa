<?php

namespace Modules\Admin;

use Nette\Application\UI\Form;

/**
 * Controls product manipulation on admin side
 */
class EmployeePresenter extends BasePresenter
{

	/**
	 * Checks if user has necessary access rights
	 *
	 * User without admin privileges can see only his own edit page
	 *
	 * @throws \Nette\Application\AbortException On redirect
	 */
	public function startup()
	{
		parent::startup();

		if ($this->getAction() !== 'edit' && !$this->getUser()->getAccountEntity()->isAdmin()) {
			$this->flashMessage('Nemáte práva pro přístup na tuto stránku', 'danger');
			$this->redirect('Home:default');
		}
	}

	/**
	 * Renders the default view
	 */
	public function renderDefault()
	{
		$url = $this->getHttpRequest()->getUrl();
		$this->template->accounts = $this->account->findAllAccounts();
		$this->template->tokens = $this->account->findAllActiveTokens();
		$this->template->baseUrl = $url->getHostUrl() . $this->link(':Front:Account:register');
	}

	/**
	 * Renders view to edit account(employee)
	 *
	 * @param int $id Id of the account to edit
	 * @throws \Nette\Application\AbortException On redirect
	 */
	public function renderEdit($id)
	{
		$account = $this->getUser()->getAccountEntity();
		if (!$account->isAdmin() && $account->getId() != $id) {
			$this->flashMessage('Nemáte práva pro přístup na tuto stránku', 'danger');
			$this->redirect('Home:default');
		}

		$this->template->id = $id;
		$employee = $this->account->find($id);
		if ($employee === null) {
			$this->flashMessage('Zaměstnanec #' . $id . ' nenalezen', 'danger');
			$this->redirect('Employee:default');
		}

		/** @var Form $form */
		$form = $this->getComponent('editEmployee');
		$form->setDefaults([
			'mail' => $employee->getMail(),
			'admin' => $employee->isAdmin()
		]);
	}

	/**
	 * Creates a new RegisterToken
	 *
	 * @param bool $ajax True if requesting as Ajax, false for normal request with redirect
	 * @throws \Exception When creating fails due to database problems
	 * @throws \Nette\Application\AbortException On redirect
	 */
	public function actionAddToken($ajax = false)
	{
		$token = $this->account->generateToken();
		$url = $this->getHttpRequest()->getUrl();
		$message = $url->getHostUrl() . $this->link(':Front:Account:register', $token);
		if ($ajax)
			die('{"link": "' . $message . '", "id": "' . $this->account->getToken($token)->getId() . '"}');

		$this->flashMessage('Odkaz vytvořen - <a href="' . $message . '">' . $message . '</a>', 'success');
		$this->redirect('default');
	}

	/**
	 * Removes account with the given id
	 *
	 * @param int $id Id of the account to remove
	 * @throws \Exception When removing fails due to database problems
	 * @throws \Nette\Application\AbortException On redirect
	 */
	public function actionRemove($id)
	{
		$employee = $this->account->find($id);
		if ($employee === null) {
			$this->flashMessage('Zaměstnanec #' . $id . ' nenalezen', 'danger');
			$this->redirect('Employee:default');
		} elseif ($id === $this->getUser()->getId()) {
			$this->flashMessage('Nemůžete odebrat svůj vlastní účet', 'danger');
			$this->redirect('Employee:default');
		}
		if ($this->account->remove($id)) {
			$this->flashMessage('Zaměstnanec byl úspěšně odebrán', 'success');
			$this->redirect('Employee:default');
		} else {
			$this->flashMessage('Došlo k chybě', 'danger');
			$this->redirect('Employee:default');
		}
	}

	/**
	 * Marks a RegisterToken as used
	 *
	 * @param int $id Id of the token to remove
	 * @throws \Exception When removing fails due to database problems
	 * @throws \Nette\Application\AbortException On redirect
	 */
	public function actionRemoveToken($id)
	{
		$token = $this->account->findToken($id);
		if ($token === null) {
			$this->flashMessage('Odkaz #' . $id . ' nenalezen', 'danger');
			$this->redirect('Employee:default');
		}

		if ($this->account->removeToken($id)) {
			$this->flashMessage('Odkaz byl odebrán', 'success');
			$this->redirect('Employee:default');
		} else {
			$this->flashMessage('Došlo k chybě', 'danger');
			$this->redirect('Employee:default');
		}
	}

	/**
	 * Creates a form for editing an employee
	 *
	 * @return Form
	 */
	public function createComponentEditEmployee()
	{
		$form = new Form();

		$form->addText('mail', 'E-Mail')
			->setRequired('Zadejte E-Mail')
			->addRule(Form::EMAIL, 'E-Mailová adresa je ve špatném tvaru')
			->addRule(Form::MAX_LENGTH, 'Maximální povolená délka e-mailové adresy je 128 znaků', 128);
		$form->addCheckbox('admin', 'Admin');

		$form->addSubmit('submit', 'Uložit');
		$form->addProtection('Platnost formuláře vypršela');

		$form->onSuccess[] = [$this, 'editEmployee'];

		return $form;
	}

	/**
	 * Handles successful submitting of 'editEmployee' form
	 *
	 * Fetches the form data, checks for duplicities and saves the edited account
	 *
	 * @param Form $form The submitted form
	 * @throws \Exception When editing fails due to database problems
	 * @throws \Nette\Application\AbortException On redirect
	 */
	public function editEmployee(Form $form)
	{
		$values = $form->getValues();

		if ($this->getUser()->getId() === (int)$this->getParameter('id') && !$values->admin) {
			$this->flashMessage('Nemůžete odebrat práva sám sobě', 'danger');
			$this->redirect('this');
		}
		if ($this->account->update($this->getParameter('id'), $values)) {
			$this->flashMessage('Údaje o zaměstnanci úspěšně upraveny', 'success');
			$this->redirect('Employee:default');
		} else {
			$this->flashMessage('Došlo k chybě při ukládání', 'danger');
			$this->redirect('this');
		}
	}

}
