<?php

namespace Modules\Admin;

/**
 * Controls admin dashboard
 */
class HomePresenter extends BasePresenter
{

	/**
	 * Renders the default view
	 */
	public function renderDefault()
	{
		$this->template->stats = $this->product->getStats();
	}

}
