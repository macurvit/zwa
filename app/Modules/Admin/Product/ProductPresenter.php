<?php

namespace Modules\Admin;

use Nette\Application\UI\Form;

/**
 * Controls products manipulation on admin side
 */
class ProductPresenter extends BasePresenter
{

	/**
	 * Renders the default view
	 */
	public function renderDefault()
	{
		$this->template->products = $this->product->findAll();
	}

	/**
	 * Renders the view to edit a product
	 *
	 * @param int $id Id of the product to edit
	 * @throws \Nette\Application\AbortException On redirect
	 */
	public function renderEdit($id)
	{
		$this->template->id = $id;
		$product = $this->product->find($id);
		if ($product === null) {
			$this->flashMessage('Produkt #' . $id . ' neexistuje', 'danger');
			$this->redirect('Product:default');
		}

		$categories = [];
		foreach ($product->getCategories() as $category) {
			$categories[] = $category->getId();
		}

		/** @var Form $form */
		$form = $this->getComponent('editProduct');
		$form->getComponent('abbr')->setAttribute('readonly');
		$form->setDefaults([
			'abbr' => $product->getAbbr(),
			'name' => $product->getName(),
			'description' => $product->getDescription(),
			'price' => $product->getPrice(),
			'categories' => $categories,
		]);
	}

	/**
	 * Marks product with given id as canceled (removed)
	 *
	 * @param int $id Id of the product to remove
	 * @throws \Exception When removing fails due to database problems
	 * @throws \Nette\Application\AbortException On redirect
	 */
	public function actionRemove($id)
	{
		$product = $this->product->find($id);
		if ($product === null) {
			$this->flashMessage('Produkt  #' . $id . ' nenalezen', 'error');
			$this->redirect('Product:default');
		}

		if ($this->product->removeProduct($id)) {
			$this->flashMessage('Produkt byl úspěšně odebrán z nabídky', 'success');
			$this->redirect('Product:default');
		}
	}

	/**
	 * Marks product with given id as unavailable
	 *
	 * @param int $id Id of the product to mark unavailable
	 * @throws \Exception When saving fails due to database problems
	 * @throws \Nette\Application\AbortException On redirect
	 */
	public function actionMarkUnavailable($id)
	{
		$this->checkIfExists($id);

		if ($this->product->markUnavailable($id)) {
			$this->flashMessage('Produkt #' . $id . ' označen jako nedostupný', 'success');
			$this->redirect('Product:default');
		}
	}

	/**
	 * Marks product with given id as available
	 *
	 * @param int $id Id of the product to mark available
	 * @throws \Exception When saving fails due to database problems
	 * @throws \Nette\Application\AbortException On redirect
	 */
	public function actionMarkAvailable($id)
	{
		$this->checkIfExists($id);

		if ($this->product->markAvailable($id)) {
			$this->flashMessage('Produkt #' . $id . ' označen jako dostupný', 'success');
			$this->redirect('Product:default');
		}
	}

	/**
	 * Validates submitted form for duplicities
	 *
	 * @param Form $form The submitted form
	 * @return Form The submitted form with checked errors for duplicity
	 */
	public function validateDuplicity(Form $form)
	{
		$values = $form->getValues();
		if ($form->getName() === 'addProduct') {
			if ($this->product->isDuplicate($values->abbr))
				$form->addError('Produkt s touto zkratkou již existuje');
		} elseif ($form->getName() === 'editProduct') {
			$product = $this->product->find($this->getParameter('id'));
			if ($product->getAbbr() !== $values->abbr && $this->product->isDuplicate($values->abbr))
				$form->addError('Produkt s touto zkratkou již existuje');
		}

		return $form;
	}

	/**
	 * Creates a form for adding a product
	 *
	 * @return Form
	 */
	public function createComponentAddProduct()
	{
		return $this->createProductForm('addProduct', 'Přidat');
	}

	/**
	 * Handles successful submitting of 'addProduct' form
	 *
	 * Fetches the form data, checks for duplicates and saves new product to database
	 *
	 * @param Form $form The submitted form
	 * @throws \Exception When saving fails due to database problems
	 * @throws \Nette\Application\AbortException On redirect
	 */
	public function addProduct(Form $form)
	{
		$values = $form->getValues();

		if ($this->product->isDuplicate($values->abbr)) {
			$this->flashMessage('Produkt s takovou zkratkou již existuje', 'error');
			$this->redirect('this');
		}

		if ($this->product->addProduct($values))
			$this->flashMessage('Produkt úspěšně přidán', 'success');

	}

	/**
	 * Creates a form for editing a product
	 *
	 * @return Form
	 */
	public function createComponentEditProduct()
	{
		return $this->createProductForm('editProduct', 'Uložit');
	}

	/**
	 * Handles successful submitting of 'editProduct' form
	 *
	 * Fetches the form data and updates corresponding product to match the new data, saves to database
	 *
	 * @param Form $form The submitted form
	 * @throws \Exception When saving fails due to database problems
	 * @throws \Nette\Application\AbortException On redirect
	 */
	public function editProduct(Form $form)
	{
		$values = $form->getValues();

		if ($this->product->updateProduct($this->getParameter('id'), $values)) {
			$this->flashMessage('Produkt úspěšně upraven', 'success');
			$this->redirect('Product:default');
		} else {
			$this->flashMessage('Došlo k chybě při upravování', 'error');
			$this->redirect('this');
		}
	}

	/**
	 * Creates basic product form for both adding and editing
	 *
	 * @param string $successCallback Name of the function to call on successful form submit
	 * @param string $submitName Value of the submit input in form
	 * @return Form
	 */
	private function createProductForm($successCallback, $submitName)
	{
		$form = new Form();
		$categories = $this->category->findAllAsArray();

		$form->addText('abbr', 'Zkratka')
			->setRequired('Musíte zadat zkratku')
			->addRule(Form::MAX_LENGTH, 'Zkratka může být dlouhá maximálně 32 znaků', 32)
			->addRule(Form::PATTERN, 'Pro zkratku požijte pouze znaky A-Z, a-z, 0-9 a pomlčku', '[A-Za-z0-9-]+')
			->setAttribute('placeholder', 'kartacek-modra');

		$form->addText('name', 'Název')
			->setRequired('Musíte zadat název')
			->addRule(Form::MAX_LENGTH, 'Název může být dlouhý maximálně 64 znaků', 64)
			->setAttribute('placeholder', 'Zubní kartáček (modrý)');
		$form->addText('price', 'Cena (v kč)')
			->setRequired('Musíte zadat cenu')
			->setType('number')
			->setAttribute('placeholder', '100')
			->addRule(Form::RANGE, 'Cena musí být mezi 0 a 10 000 000 kč', [0, 10e7]);

		$form->addTextArea('description', 'Popis')
			->setAttribute('placeholder', 'Velice kvalitní zboží.');
		$form->addMultiSelect('categories', 'Kategorie', $categories)
			->setRequired('Musíte přiřadit alespoň jednu kategorii');

		$form->addUpload('image', 'Obrázek')
			->setRequired(false)
			->addRule(Form::IMAGE, 'Obrázek musí být ve formátu JPEG, GIF nebo PNG')
			->addRule(Form::MAX_FILE_SIZE, 'Maximální velikost obrázku je 5MB', 5 * 1024 * 1024);

		$form->addSubmit('submit', $submitName);
		$form->addProtection('Platnost formuláře vypršela');

		$form->onValidate[] = [$this, 'validateDuplicity'];
		$form->onSuccess[] = [$this, $successCallback];

		return $form;
	}

	/**
	 * Checks if product with given id exists
	 *
	 * @param int $id The id to check for
	 * @throws \Nette\Application\AbortException On redirect
	 */
	private function checkIfExists($id)
	{
		$product = $this->product->find($id);
		if ($product === null) {
			$this->flashMessage('Produkt  #' . $id . ' nenalezen', 'error');
			$this->redirect('Product:default');
		}
	}

}
