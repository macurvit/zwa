<?php

namespace Modules\Front;

use Entities\AccountEntity;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;

/**
 * Controls account actions
 */
class AccountPresenter extends BasePresenter
{

	/**
	 * Logs out the currently logged in user
	 *
	 * @throws \Nette\Application\AbortException On redirect
	 */
	public function actionLogout()
	{
		if (!$this->getUser()->isLoggedIn())
			$this->redirectHome();

		$this->getUser()->logout(true);
		$this->flashMessage('Byl jste úspěšně odhlášen', 'success');
		$this->redirectHome();
	}

	/**
	 * Renders the login view
	 *
	 * @throws \Nette\Application\AbortException On redirect
	 */
	public function renderLogin()
	{
		if ($this->getUser()->isLoggedIn())
			$this->redirectHome();
	}

	/**
	 * Renders the register view
	 *
	 * Requires valid register token, redirects to homepage if invalid token is given
	 *
	 * @param string $id Valid register token
	 * @throws \Nette\Application\AbortException On redirect
	 */
	public function renderRegister($id)
	{
		if ($this->getUser()->isLoggedIn())
			$this->redirectHome();

		$token = $this->account->getToken($id);
		if ($token === null)
			$this->redirectHome();

		if ($token->isUsed())
			$this->redirectHome();
	}

	/**
	 * Creates login form
	 *
	 * @return Form
	 */
	public function createComponentLogin()
	{
		$form = new Form();

		$form->addText('username', 'Uživatelské jméno', null, 32)
			->setRequired('Zadejte uživatelské jméno');
		$form->addPassword('password', 'Heslo', null, 42)
			->setRequired('Zadejte heslo');
		$form->addSubmit('submit', 'Přihlásit');

		$form->onValidate[] = [$this, 'checkLogin'];
		$form->onSuccess[] = [$this, 'login'];

		return $form;
	}

	/**
	 * Validates login form
	 *
	 * Checks for existence of account and validates password
	 *
	 * @param Form $form The submitted form
	 * @return Form The submitted form, validated
	 */
	public function checkLogin(Form $form)
	{
		$values = $form->getValues();

		$account = $this->account->findOneBy(['username' => $values->username]);
		if ($account === null)
			$form->addError('Uživatel s takovým jménem neexistuje');
		elseif (!password_verify($values->password, $account->getPassword()))
			$form->addError('Nesprávné heslo');

		return $form;
	}

	/**
	 * Handles successful submitting of 'login' form
	 *
	 * Checks validity of submitted data and logs user in
	 *
	 * @param Form $form The submitted form
	 * @throws \Nette\Application\AbortException On redirect
	 */
	public function login(Form $form)
	{
		$values = $form->getValues();

		try {
			$this->user->login($values->username, $values->password);
			$this->flashMessage('Byl jste úspěšně přihlášen', 'success');
			$this->redirectHome();
		} catch (AuthenticationException $e) {
			$this->flashMessage('Při přihlašování došlo k chybě', 'danger');
			$this->redirect('this');
		}
	}

	/**
	 * Creates register form
	 *
	 * @return Form
	 */
	public function createComponentRegister()
	{
		$form = new Form();

		$form->addText('username', 'Uživatelské jméno', null, 32)
			->setRequired('Zadejte uživatelské jméno')
			->addRule(Form::PATTERN, 'Uživatelské jméno obsahuje nepovolené znaky, použijte pouze znaky anglické abecedy a číslice 0-9', '[a-zA-Z0-9]+')
			->setAttribute('placeholder', 'mojejmeno');
		$form->addPassword('password', 'Heslo', null, 42)
			->setRequired('Zadejte heslo')
			->addRule(Form::LENGTH, 'Délka hesla musí být mezi 6 a 42 znaky', [6, 42]);
		$form->addPassword('passwordCheck', 'Kontrola hesla', null, 42)
			->setRequired('Zadejte heslo pro kontrolu');
		$form->addText('mail', 'E-Mail')
			->setRequired('Zadejte E-Mail')
			->addRule(Form::EMAIL, 'E-Mailová adresa je ve špatném tvaru')
			->addRule(Form::MAX_LENGTH, 'Maximální povolená délka e-mailové adresy je 128 znaků', 128);
		$form->addSubmit('submit', 'Zaregistrovat');

		$form->onValidate[] = [$this, 'checkRegister'];
		$form->onSuccess[] = [$this, 'register'];

		return $form;
	}

	/**
	 * Validates register form
	 *
	 * @param Form $form The submitted form
	 * @return Form The submitted form, validated
	 * @throws \Exception If check for invalid field is requested
	 */
	public function checkRegister(Form $form)
	{
		$values = $form->getValues();
		if ($values->password !== $values->passwordCheck)
			$form->addError('Zadaná hesla musí být stejná');

		if ($this->account->isDuplicate('username', $values->username))
			$form->addError('Uživatelské jméno je již použito');

		if ($this->account->isDuplicate('mail', $values->mail))
			$form->addError('E-Mail je již použit');

		return $form;
	}

	/**
	 * Handles successful submitting of 'register' form
	 *
	 * Creates a new account, saves to database and logs in the newly registered user
	 *
	 * @param Form $form The submitted form
	 * @throws \Exception If saving fails due to database problems
	 * @throws \Nette\Application\AbortException On redirect
	 */
	public function register(Form $form)
	{
		$values = $form->getValues();
		$account = new AccountEntity();

		$account->setUsername($values->username);
		$account->setPassword(password_hash($values->password, PASSWORD_BCRYPT));
		$account->setMail($values->mail);

		if ($this->account->isFirst())
			$account->setAdmin(true);
		// @todo add more checks on PW, Mail & username
		$this->account->save($account);

		$token = $this->account->getToken($this->getParameter('id'));
		$token->setUsed(true);
		$this->account->saveToken($token);

		$this->getUser()->login($values->username, $values->password);
		$this->flashMessage('Byl jste úspěšně zaregistrován', 'success');
		$this->redirect(':Front:Home:default');
	}

}
