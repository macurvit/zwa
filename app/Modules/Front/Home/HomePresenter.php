<?php

namespace Modules\Front;

use Models\Product;

/**
 * Controls homepage rendering
 */
class HomePresenter extends \GlobalPresenter
{
	/**
	 * Model for interaction with products
	 *
	 * @var Product
	 */
	protected $product;

	/**
	 * Injects the required service
	 *
	 * @param Product $product
	 */
	public function injectServices(Product $product)
	{
		$this->product = $product;
	}

	/**
	 * Renders the default view
	 */
	public function renderDefault()
	{
		$this->template->products = $this->product->getHomepageProducts();
	}

}
