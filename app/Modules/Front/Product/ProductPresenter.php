<?php

namespace Modules\Front;

use Models\Category;
use Models\Product;
use Nette\Application\UI\Form;
use Nette\Utils\Paginator;

/**
 * Controls interaction with products
 */
class ProductPresenter extends BasePresenter
{

	/**
	 * Model for interaction with products
	 *
	 * @var Product
	 */
	protected $product;

	/**
	 * Model for interaction with categories
	 *
	 * @var Category
	 */
	protected $category;

	/**
	 * Injects required services
	 *
	 * @param Product $product
	 * @param Category $category
	 */
	public function injectService(Product $product, Category $category)
	{
		$this->product = $product;
		$this->category = $category;
	}

	/**
	 * Renders the list view with given parameters
	 *
	 * @param int $page The page to view
	 * @param int $items Items per page
	 * @param bool|null $available True if only available products should be shown
	 * @param float|null $priceStart The minimal price of products to show
	 * @param float|null $priceEnd The maximal price of products to show
	 * @param string|null $category Id of category from which to show products
	 * @param string $orderBy Field to order the product by
	 * @param string $orderType Products order direction (ascending x descending)
	 */
	public function renderList($page = 1, $items = 5, $available = null, $priceStart = null, $priceEnd = null, $category = 0, $orderBy = 'releaseDate', $orderType = 'DESC')
	{
		$filters = [
			'available' => (bool) $available,
			'priceStart' => $priceStart,
			'priceEnd' => $priceEnd,
			'category' => $category,
			'orderBy' => $orderBy,
			'orderType' => $orderType,
		];

		$productsCount = $this->product->getActiveProductsCount($filters);

		$paginator = new Paginator();
		$paginator->setItemCount($productsCount);
		$paginator->setItemsPerPage($items);
		$paginator->setPage($page);

		$this->template->products = $this->product->findActiveProducts($paginator->getLength(), $paginator->getOffset(), $filters);
		$this->template->paginator = $paginator;
		$this->template->filters = $filters;
		$this->template->items = $items;

		/** @var Form $form */
		$form = $this->getComponent('filter');
		$form->setDefaults($filters);
	}

	/**
	 * Renders the detail view of a product
	 *
	 * @param int $id Id of the product to show
	 * @throws \Nette\Application\AbortException On redirect
	 */
	public function renderDetail($id)
	{
		if (!$id)
			$this->redirect('Product:list');

		$product = $this->product->find($id);
		if ($product === null) {
			$this->flashMessage('Produkt nenalezen', 'danger');
			$this->redirectHome();
		}

		$this->template->product = $product;
	}

	/**
	 * Creates the filter form
	 *
	 * Generates filter inputs and redirects to corresponding URL on successful submit
	 *
	 * @return Form
	 */
	public function createComponentFilter()
	{
		$form = new Form();

		$categories = [0 => 'Libovolná'] + $this->category->findAllAsArray();

		$form->addSelect('category', 'Kategorie', $categories);
		$form->addCheckbox('available', 'Pouze dostupné produkty');
		$form->addText('priceStart', 'Cena od (Kč)')
			->setType('number');
		$form->addText('priceEnd', 'Cena do (Kč)')
			->setType('number');
		$form->addSelect('orderBy', 'Seřadit dle', ['releaseDate' => 'Data přidání', 'price' => 'Ceny', 'stockEmptyDate' => 'Data nedostupnosti']);
		$form->addSelect('orderType', 'Seřadit', ['DESC' => 'Sestupně', 'ASC' => 'Vzestupně']);

		$form->addSubmit('submit', 'Zobrazit');

		$form->onSuccess[] = function (Form $form) {
			$values = $form->getValues();
			$this->redirect('this', ['page' => $this->getParameter('page'), 'items' => $this->getParameter('items'), 'available' => ($values->available === true) ? true : null, 'priceStart' => $values->priceStart, 'priceEnd' => $values->priceEnd, 'orderBy' => $values->orderBy, 'orderType' => $values->orderType, 'category' => $values->category]);
		};

		return $form;
	}

}
