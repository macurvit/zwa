<?php

use Models\Account;

/**
 * Base presenter for the whole application
 */
class GlobalPresenter extends \Nette\Application\UI\Presenter
{

	/**
	 * Model for interaction with accounts
	 * @var Account
	 */
	protected $account;

	/**
	 * Redirects the use to homepage
	 *
	 * @throws \Nette\Application\AbortException
	 */
	protected function redirectHome()
	{
		$this->redirect(':Front:Home:default');
	}

	/**
	 * Returns the User instance
	 *
	 * Exists here only for IDE type hinting
	 *
	 * @return \Models\User|\Nette\Security\User
	 */
	public function getUser()
	{
		return parent::getUser();
	}

	/**
	 * Injects the account service
	 *
	 * @param Account $account
	 */
	public function injectAccount(Account $account)
	{
		$this->account = $account;
	}

	/**
	 * Assigns instance of AccountEntity to logged in user
	 */
	public function startup()
	{
		parent::startup();

		if ($this->getUser()->isLoggedIn() && $this->getUser()->getAccountEntity() === null)
			$this->getUser()->setAccountEntity($this->account->find($this->getUser()->getId()));
	}

	/**
	 * Formats layout template file names
	 *
	 * For better application structure
	 *
	 * @return array
	 */
	public function formatLayoutTemplateFiles() {
		$name = $this->getName();
		$layout = $this->layout ? $this->layout : 'layout';
		$dir = dirname($this->getReflection()->getFileName());
		$dir = is_dir("$dir/templates") ? $dir : dirname($dir);
		$list = array(
			"$dir/templates/@$layout.latte",
			"$dir/templates/@$layout.phtml",
			"$dir/../@$layout.latte",
		);
		do {
			$dir = dirname($dir);
			$list[] = "$dir/templates/@$layout.latte";
			$list[] = "$dir/templates/@$layout.phtml";
		} while ($dir && ($name = substr($name, 0, strrpos($name, ':'))));
		return $list;
	}


	/**
	 * Formats view template file names
	 *
	 * For better application structure
	 *
	 * @return array
	 */
	public function formatTemplateFiles() {
		$dir = dirname($this->getReflection()->getFileName());
		$dir = is_dir("$dir/templates") ? $dir : dirname($dir);
		return array(
			"$dir/templates/$this->view.latte",
			"$dir/templates/$this->view.phtml",
		);
	}

}
