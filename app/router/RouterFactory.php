<?php

namespace App;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

/**
 * Generates fancy URLs
 */
class RouterFactory
{
	use Nette\StaticClass;

	/**
	 * Creates the router for all application requests
	 *
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;
		$router[] = new Route('<module=Front>/<presenter=Home>/<action=default>[/<id>]');
		return $router;
	}
}
