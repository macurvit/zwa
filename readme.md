Semestrální práce z předmětu ZWA
=================

Tento projekt obsahuje zdrojové kódy k semestrální práci z předmětu ZWA (B6B39ZWA). Spuštěný web je k nalezení na adrese [http://zwa.vmacura.cz](http://zwa.vmacura.cz)

Zadání
------------

**Název:** Katalog produktů

**Cíl:** Umožnit zákazníkům prohlížet katalog určité frimy

**Uživatelské role:**
- Nepřihlášený uživatel (zákazník)
- Přihlášení uživatelé
  - Zaměstnanec firmy
  - Majitel firmy/administrativa
  
**Funkce:**

Produkt obsahuje popis, cenu, dostupnost, čas přidání do nabídky, čas nedostupnosti, čas stažení z prodeje, fotku a je přiřazen do kategorií (alespoň do jedné)

Na hlavní straně jsou zobrazeny 4 poslední přidané produkty, které mají fotku, jsou dostupné a nejsou stažené z prodeje

Zaměstnance (uživatele) může přidávat a upravovat pouze majitel/administrativa

**Funkce podle rolí:**

- Nepřihlášení uživatelé
  - Mohou prohlížet produkty, filtrovat je a řadit
- Zaměstnanci
  - Jako nepřihlášení + mohou přidávat a upravovat kategorie a produkty
- Administrativa/majitel
  - Jako zaměstnanci + mohou generovat odkaz pro registraci a upravovat zaměstnance
- registrace je možná pouze přes administrativou vygenerovaný odkaz

**Stránky:**

- Hlavní strana
- Seznam produktů
- Detail produktu
- Administrace
  - Produkt - přidat/upravit(/odebrat)
  - Kategorie - přidat/upravit(/odebrat)
  - Zaměstnanec - přidat/upravit(/odebrat)
 




Instalace
------------

**Požadavky:** PHP 7.0 nebo vyšší, nainstalován composer a je dostupný v PATH

[Instalace composer-u](https://doc.nette.org/composer)

Projekt nainstalujete následujícími příkazy (Linux):

```bash
> git clone git@gitlab.fel.cvut.cz:macurvit/zwa.git
> cd ./zwa/
> composer install
> cp app/config/config.local.example.neon app/config/config.local.neon
```

Ujistěte se, že webserver má právo zapisovat do složek `temp/` a `log/` .

Zapište své údaje pro připojení k databázi do souboru `app/config/config.local.neon`.

Spusťte příkazy ze souboru `app/init.sql` ve Vaší databázi.

### Je hotovo ###

První registraci proveďte na adrese `HOST/front/account/register/init`, a jako prvnímu registrovanému uživateli Vám automaticky budou připsána práva administrátora.


Použité technologie
----------------------

Ve tvaru
- Název - Licence - Popis

- [Nette](https://nette.org/) - BSD/GNU(GPL) - Hlavní kostra celé aplikace, od routování, přes stránkování až po tvorbu formulářů včetně integrované ochrany proti XSS, šablonovací systém Latte... viz web
- [Doctrine implementace do Nette](https://github.com/Kdyby/Doctrine) - BSD/GNU(GPL) Mapování databáze na PHP objekty včetně jejich relací
- [Bootstrap](https://getbootstrap.com/) - MIT - Většina CSS, layout aplikace
- [jQuery](https://jquery.com/) - MIT - Pro plnou funkčnost Bootstrap-u
- [Composer](https://getcomposer.org/) - MIT - Pro deklaraci závislostí projektu

Poznámky
------

- Ajax je využit pro vytváření nových a mazání starých odkazů pro registraci (`/admin/employee/default`)
- "Zajímavosti v JavaScriptu": Možnost zavírat všechna oznámení ("flash message") po vykonání nějaké akce, možnost schovat a zobrazit filtry u seznamu produktů (`/front/product/list`)
- "Zajímavost v CSS": Mediaqueries pro úpravy Bootstrapu (`/styles/layout.css`)
- Vygenerovaný PHPDoc je k zobrazení na adrese [http://zwa.vmacura.cz/api/](http://zwa.vmacura.cz/api)
